Configuration
=============

Watchghost configuration lives within four files within a single
directory. These files describe our monitored infrastructure. Each file
contains one kind of configured element.

The four kind of configuration element (and their filenames) are:

- ``servers``
- ``groups``
- ``watchers``
- ``loggers``

The default directory is ``~/.config/watchghost``. This folder can be changed
using the ``config`` option of Watchghost executables.

The format of these files is TOML and each file contains a list of elements.


Servers
-------

.. autoclass:: watchghost.config.Server


Groups
------

A Group is only represented by its name. This name is a string that must be
unique amongst all servers and groups. Values must be a list of valid server
names.

Example:

.. code-block:: toml

  postgres = ["jupiter", "vulcan"]
  linux = ["jupiter", "ceres"]
  windows = ["vulcan"]

This example defines three groups named ``postgres`` (including the ``jupiter``
and ``vulcan`` servers), ``linux`` (including the ``jupiter`` and ``ceres``
servers), and ``windows`` (including the ``vulcan`` server).


Watchers
--------

.. autoclass:: watchghost.services.Service

`See the list of builtin services used by watchers <watchers.html>`_


Loggers
-------

.. autoclass:: watchghost.loggers.Logger

`See the list of builtin loggers <loggers.html>`_
