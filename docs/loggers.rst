Builtin Loggers
===============

Logger
------

Shared configuration to all builtin loggers. Every other logger heritate from this one.

Default configuration:

.. code-block:: toml

  [logger]
  status = null
  only_hard = false
  only_changes = true

:status: a list of watcher's status at which events are logged
:only_hard: If set to ``true``, activate logger only if persistant state. If set to ``false`` activate logg at every watcher changes.
:only_changes: If set to ``true``, activate logger only if ther is changes compare to the previous version. If set to ``false`` activate logger every time.


Console
-------

How to :

.. code-block:: toml

  [console]
  type = "Console"

Default configuration:

.. code-block:: toml

  message =
    "{watcher.description} on {watcher.server.name} "
    "({watcher.last_check_result})"
  filename = null

:message: The message given by the logger.
:filename: If no filename is specified, print on standard output. If set to something (ex: log.log) logger store in this file.




Sendmail
--------

How to :

.. code-block:: toml

  [sendmail]
  type = "Sendmail"

Default configuration:

.. code-block:: toml

  status = ["warning", "error", "critical", "unknown"]
  from = "no-reply@example.com"
  to = "no-reply@example.com"
  subject = "Error for {watcher.description} on {watcher.server.name}"
  message =
      "Error for {watcher.description} on {watcher.server.name}.\n\n"
      "{watcher.last_check_result}"

:status: available statuses for this logger
:from: mail expeditor
:to: mail receiver
:subject: mail subject
:message: mail content


Smtp
----

How to :

.. code-block:: toml

  [smtp]
  type = "Smtp"

Default configuration

.. code-block:: toml

  status = ["info", "warning", "error", "critical", "unknown"]
  from = "no-reply@example.com"
  to = "no-reply@example.com"
  subject = "{watcher.status} for {watcher.description} on {watcher.server.name}"
  message =
      "{watcher.status} for {watcher.description} on {watcher.server.name}.\n\n "
      "{watcher.last_check_result}"
  smtp_host = "localhost"
  smtp_port = 587
  smtp_starttls = true
  smtp_username = null
  smtp_password = null

:status: available statuses for this logger
:from: mail expeditor
:to: mail receiver
:subject: mail subject
:message: mail content
:smtp_starttls: Set to ``true`` to start a tls smtp connection. Set to ``false`` otherwise.


SmsOVH
------

How to :

.. code-block:: toml

  [smsovh]
  type = "SmsOVH"

Default configuration

.. code-block:: toml

  status = ["info", "warning", "error", "critical", "unknown"]
  base_url = "https://www.ovh.com/cgi-bin/sms/http2sms.cgi?"
  sms_account = null
  sms_login = null
  sms_password = null
  sms_from = null
  sms_to = null
  timeout = false
  message =
      "[{watcher.server.name}]"
      "'{watcher.status}' status for '{watcher.description}'"

Command
-------

This logger permits to call a command on the server on which watchghost is installed.

The logger calls the given command and send a JSON on stdin. This JSON is the serialization of the watcher. The command should read stdin and parse it from JSON in order to do something.

Here is an example configuration:

.. code-block:: toml

  [a_command]
  type = "Command"
  status = ["info", "warning", "error", "critical", "unknown"]
  only_hard = true
  command = "/usr/bin/a_script.py"

Here is an example executable written in Python:

.. code-block:: python

  #!/usr/bin/env python

  import sys
  import json

  content = sys.stdin.read()
  watcher = json.loads(content)

  # do something with watcher variable

InfluxDB
--------

This logger stores logs in an `InfluxDB <https://www.influxdata.com/>`_
database.

Here is the default configuration:

.. code-block:: toml

  [influx]
  type = InfluxDB
  host = localhost
  port = 8086
  username = root
  password = root
  database = watchghost
  only_changes = false

Database tags are:

- watcher's tags,
- status,
- server,
- name.

Database fields are:

- watcher's response fields,
- status.
