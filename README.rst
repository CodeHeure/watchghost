==========
WatchGhost
==========

.. image:: https://readthedocs.org/projects/watchghost/badge/?version=latest&style=plastic

Your invisible but loud monitoring pet

Quickstart
==========

.. code-block:: shell

  pip install watchghost
  watchghost
  $NAVIGATOR http://localhost:8888
  $EDITOR ~/.config/watchghost/*

Code on watchghost
==================

.. code-block:: shell

  git clone https://gitlab.com/localg-host/watchghost.git
  cd watchghost
  pip install -e .
  python -m watchghost
